package com.tony;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        int i = 2,j = 4,k = 6;
        k = j = i = 10;
        System.out.println("i " + i);
        System.out.println("j " + j);
        System.out.println(k = 20);
        System.out.println("k " + k);
    }

    public static void exerciseLambd() {

        //给出一个String类型的数组，找出其中各个素数，并统计其出现次数
        String[] strs = {"1","12","12","32","3","34","34","56","1","45"};
        Arrays.asList(strs).stream().filter(e->{return true;});
    }

    public static void leanrnLambd1() {
        List<String> ol = Arrays.asList("asd", "123", "asd2", "12", "213", "435");
        ol.stream().filter((e) -> {
            char[] ch = e.toCharArray();
            for (char c : ch) {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }).map(e -> new Integer(e)).distinct().sorted((e1, e2) -> e1.compareTo(e2)).collect(Collectors.toMap(e -> {return "key"+e;}, e -> {return "value"+e;})).forEach((e1,e2)->{
            System.out.print("key is : " + e1);
            System.out.println("  value is : " + e2);
        });

    }
}
