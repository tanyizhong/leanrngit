package com.tony;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by peony on 2017/12/5.
 */
public class TestMain {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,873,76434653,44,13,4,54);
        list.sort((e1,e2) -> {return e1.compareTo(e2);});
        Object[] objs = list.stream().filter((e) -> {
            return e > 100;
        }).toArray();
        System.out.println(objs.length + " objs");
        System.out.println(list);
    }
}
